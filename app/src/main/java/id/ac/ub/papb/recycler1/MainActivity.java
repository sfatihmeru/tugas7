package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainActivity extends AppCompatActivity implements SearchManager.OnCancelListener, View.OnClickListener {

    RecyclerView rv1;
    public static String TAG = "RV1";
    Button _bt1;
    EditText _edtNama, _edtNim;
    ArrayList<Mahasiswa> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _bt1 = findViewById(R.id.bt1);
        _bt1.setOnClickListener(this);
        _edtNama = findViewById(R.id.etNama);
        _edtNim = findViewById(R.id.etNim);
        rv1 = findViewById(R.id.rv1);
        data = getData();
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));
    }

    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }

    private void addData(){
        Mahasiswa mhs = new Mahasiswa();
        mhs.nim = _edtNim.getText().toString();
        mhs.nama = _edtNama.getText().toString();
        data.add(mhs);
    }

    @Override
    public void onCancel() {

    }

    @Override
    public void onClick(View view) {
        if(view.getId()==_bt1.getId()){
            addData();
        }
    }
}