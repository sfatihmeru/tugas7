package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    TextView _tvNim, _tvNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        _tvNim = findViewById(R.id.tvNim2);
        _tvNama = findViewById(R.id.tvNama2);

        Bundle bundle = getIntent().getExtras();
        String _getNim = bundle.getString("NIM");
        String _getNama = bundle.getString("NAMA");
        _tvNim.setText("NIM: " + _getNim);
        _tvNama.setText("Nama: " + _getNama);
    }
}